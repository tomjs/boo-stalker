Boo! Ghostly Stalker
====================

Boo! is a ghostly-looking thing that picks a nearby avatar and follows them
around, until it pick another nearby avatar to follow.  Boo! has a tendency
to rudely shower nearby avatars with, um, goo at random times, so beware!

After unpacking, rez Boo! on the ground to start the stalking...

Boo! (usually) listens to commands on Local Chat channel 800 (start the command
with /800) and is trained to often obey them::

    stay - Causes Boo! to stop following any avatar

    roam - Allows Boo! to resume stalking a target avatar

    come - Causes Boo! to return to its owner but continue being annoying

    go - Allows Boo! to pick a new target avatar and get on with stalking it

    name <new-name> - Give Boo! a new name

There is one more emergency command that will end Boo!'s existence: poof.  This
end is a 'de-rez', not a 'take', so no changes will be saved back to your inventory.

Because Boo! is a rezzed object and not an attachment, avatars being followed that 
teleport away will of course leave Boo! behind.  This may be your only avenue of escape!

Setting the name actually sets the object name so anything Boo! says in Local
Chat will be properly attributed.  This means that if you 'take' Boo! back into
your inventory the name will be set and will still be set the next time you
rez Boo! in-world.  This also means that Boo!'s name can be set with the usual
viewer Edit tool directly.


Examples
========

Make Boo! stop following avatars::

    /800 stay

Change Boo!'s name to Leonard::

    /800 name Leonard

Send Boo! back to the Netherworlds (i.e., de-rez)::

    /800 poof


LICENSE
=======

The scripts that are included with Boo! Ghostly Stalker are licensed under
the GNU Public License v3.  The script source code may be found in my GitLab
repository at https://gitlab.com/tomjs/boo-stalker/tree/master.

Enjoy!
Layne Thomas (tomjs)
