// Copies object inventory contents to user's inventory
// v1
// License: public domain

// Configuration

string title = "Boo!";
integer owneronly = TRUE;

// State

key owner;

set_text(string text) {
    llSetText(text, <1, 1, 1>, 1.0);
}

default {

    on_rez(integer start_param) {
        set_text(title);
    }

    state_entry() {
        owner = llGetOwner();        
    }

    touch_start(integer total_number) {
        integer av;
        for (av=0;av<total_number;av++) {
            key target = llDetectedKey(av);

            if ((target != owner) && (owneronly == TRUE)) {
                // person clicking isn't owner and owneronly is set;
                llInstantMessage(target, "Sorry, only the owner is allowed to get my contents.");
            } else {
                list inventory;
                string name;
                integer num = llGetInventoryNumber(INVENTORY_ALL);
                string objectname = llGetObjectName() + " is unpacking...\n";
                integer i;

                llSetAlpha(0, 0);

                for (i=0; i<num; ++i) {
                    name = llGetInventoryName(INVENTORY_ALL, i);
                    if(llGetInventoryPermMask(name, MASK_OWNER) & PERM_COPY)
                        inventory += name;
                    else
                        llInstantMessage(target, "Cannot give asset \""+name+"\", owner lacks copy permission");
                    set_text(objectname + (string)((integer)(((i + 1.0) / num) * 100))+ "%");
                }
        
                //chew off the end off the text message.
                objectname = llGetObjectName();
        
                //we don't want to give them this script
                i = llListFindList(inventory, [llGetScriptName()]);
                if (~i)
                    inventory = llDeleteSubList(inventory, i, i);
        
                if (llGetListLength(inventory) < 1) {
                    llInstantMessage(target, "No items to offer.");
                } else {
                    llGiveInventoryList(target, objectname, inventory);
                    set_text(title);
                    name = "Your new items can be found in your inventory, in a folder called '"+ objectname +"'.";
                    llInstantMessage(target, name);
                }
            }
        }
    }

}
