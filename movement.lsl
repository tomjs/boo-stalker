// Boo! Main Control and Movement
// Version: 11
//
// Copyright 2015 Layne Thomas (tomjs)
// License: GPLv3
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


// The movement and physics is based on WaS Zombie Movement script from
// http://grimore.org/secondlife/zombies/zombie/movement
//////////////////////////////////////////////////////////
//   (C) Wizardry and Steamworks 2011, license: GPLv3   //
// Please see: http://www.gnu.org/licenses/gpl.html     //
// for legal details, rights of fair usage and          //
// the disclaimer and warranty conditions.              //
//////////////////////////////////////////////////////////


// Configuration

// The usual command listener configuration
integer LISTEN = TRUE;
integer CHANNEL = 800;

// Position relative to avitar
// This is the target location during follower movement
vector TargetOffset = <0.5,0.1,1>;

// Set TRUE to follow a flying avatar
integer CanIFly = TRUE;

// Forced to select only the owner
integer OwnerOnly = FALSE;

// Let owners name their stalker
// Empty names will be filled-in with 'Boo!'
string MyName = "";


// State

// Owner key
key Owner = NULL_KEY;

// Current target avatar (if one)
key TargetKey = NULL_KEY;
string TargetName = "";

// Current position
vector CurrentPos;

// Command state
integer CanIMove = TRUE;

// Will be TRUE when close to target agent
integer NearTarget = FALSE;

// Will be TRUE when underwater
integer Underwater = FALSE;


integer listener;

start_listen() {
    llListenRemove(listener);
    if (LISTEN == TRUE)
        listener = llListen(CHANNEL, "", "", "");
}

// Integer that the script will use to detect
// the next target position it will reach.
integer targetPos;

// Orientates the primitive's positive z axis 
// towards a position and moves the primitive
// towards that position.
//
// IN: vector representing a position in region
// coordinates.
// OUT: nothing.
moveTo(vector position) {
    llTargetRemove(targetPos);
    targetPos = llTarget(position, 0.8);
    llLookAt(position, 0.6, 0.6);
    if (CanIMove == 1) {
        llMoveToTarget(position, 3.0);
    }
}

// Set up the movement engine
initialize_engine() {
    llSetStatus(STATUS_PHYSICS, FALSE);
    llSetRot(llEuler2Rot(<180., 65., 180.>*DEG_TO_RAD));
    llSetStatus(STATUS_PHYSICS, TRUE);
    llSetForce(<0,0,9.81> * llGetMass(), 0);
    llVolumeDetect(TRUE);
    llSetStatus(STATUS_BLOCK_GRAB, TRUE);
}

// set to idle position
set_idle_position() {
    llSetStatus(STATUS_PHYSICS, FALSE);
    llSetRot(llEuler2Rot(<180., 65., 180.>*DEG_TO_RAD));
}


string format_text() {
    return
        "near: " + (string)NearTarget + "\n" +
        "move: " + (string)CanIMove + "\n" +
        "home: " + (string)OwnerOnly + "\n" +
        "water: " + (string)Underwater + "\n" +
        "target: " + TargetName + "\n";
}

show_text(string msg) {
    llSetText(msg, <1,1,1>, 1.0);
}


default {

    state_entry() {
        Owner = llGetOwner();
        MyName = llGetObjectName();
        show_text("");
        start_listen();
        CurrentPos = llGetPos();
        initialize_engine();
        llSensorRepeat("", "", AGENT, 64, TWO_PI, 2);
    }

    state_exit() {
        // does this work??
        llSetStatus(STATUS_PHYSICS, FALSE);
    }

    sensor(integer num) {
        CurrentPos = llGetPos();
        show_text("");
        llSetStatus(STATUS_PHYSICS, TRUE);
        llVolumeDetect(TRUE);
        llSetStatus(STATUS_BLOCK_GRAB_OBJECT, TRUE);
        llSetStatus(STATUS_ROTATE_X|STATUS_ROTATE_Y|STATUS_ROTATE_Z, TRUE);

        // Do agent selection
        integer nagent = 0;
        if (OwnerOnly) {
            // Force to select owner
            integer i;
            for (i=0;i<num;i++) {
                key k = llDetectedKey(i);
                if (k == Owner) {
                    nagent = i;
                }
            }
        } else {
            // Pick one at random
            nagent = (integer)llFrand(--num);
        }

        // Get agent info
        TargetKey = llDetectedKey(nagent);
        if(llGetAgentInfo(TargetKey) & AGENT_FLYING && !CanIFly) {
            llSetStatus(STATUS_PHYSICS, FALSE);
            return;
        }
        TargetName = llKey2Name(TargetKey);
        vector dpos = llDetectedPos(nagent);

        // check delta, see if we are 'there'
        vector delta = dpos - CurrentPos;
        NearTarget = (llFabs(delta.x) < 1.0 && llFabs(delta.y) < 1.0);

        // Determine which particles to emit
        float water = llWater(ZERO_VECTOR);
        Underwater = (CurrentPos.z < water);

        show_text("");
        if (Underwater) {
            llMessageLinked(LINK_THIS, 4, "particles","");
        } else if (NearTarget) {
            // Be polite(er) when close to target
            llMessageLinked(LINK_THIS, 3, "particles","");
            show_text("Boo!\nI'm " + MyName);
        } else {
            if (TargetKey == Owner) {
                llMessageLinked(LINK_THIS, 2, "particles","");
            } else{
                llMessageLinked(LINK_THIS, 1, "particles","");
            }
        }

        // Go to there
        vector offset = TargetOffset * llDetectedRot(0);
        moveTo(dpos + offset);
    }

    no_sensor() {
        set_idle_position();
        show_text("");
    }

    on_rez(integer num) {
        set_idle_position();
        llResetScript();
    }

    listen(integer channel, string name, key id, string msg)  {

        if (LISTEN && channel == CHANNEL) {
            string cmd = "";
            integer idx = llSubStringIndex(msg, " ");
            if (idx == -1) {
                cmd = msg;
            } else {
                cmd = llGetSubString(msg, 0, idx-1);
                msg = llDeleteSubString(msg, 0, idx);
            }
            cmd = llToLower(cmd);
            if (cmd == "stop" || cmd == "stay") {
                CanIMove = FALSE;
                llSay(0, "I'll be right here watching " + TargetName);
            }
            if (cmd == "start" || cmd == "roam") {
                CanIMove = TRUE;
                llSay(0, TargetName + ", where are you?");
            }
            if (cmd == "fly") {
                CanIFly = TRUE;
            }
            if (cmd == "nofly") {
                CanIFly = FALSE;
            }
            if (cmd == "come" || cmd == "home" || cmd == "go home") {
                CanIMove = TRUE;
                OwnerOnly = TRUE;
            }
            if (cmd == "go" || cmd == "go away") {
                OwnerOnly = FALSE;
            }
            if (cmd == "p0") {
                llMessageLinked(LINK_THIS, 0, "particles","");
            }
            if (cmd == "p1") {
                llMessageLinked(LINK_THIS, 1, "particles","");
            }
            if (cmd == "p2") {
                llMessageLinked(LINK_THIS, 2, "particles","");
            }
            if (cmd == "p3") {
                llMessageLinked(LINK_THIS, 3, "particles","");
            }
            if (cmd == "p4") {
                llMessageLinked(LINK_THIS, 4, "particles","");
            }
            if (id == Owner) {
                // Owner-only commands
                if (cmd == "name") {
                    MyName = msg;
                    llSetObjectName(msg);
                    llSay(0, "You can all call me " + MyName + " now!");
                }
                if (cmd == "ping") {
                    llOwnerSay(
                        "I am at " + (string)llGetPos() + "\n" +
                        format_text()
                    );
                }
                if (cmd == "poof") {
                    llSay(0, "Goodbye cruel world...");
                    llDie();
                }
            }
        }
    }

}
